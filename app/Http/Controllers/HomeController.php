<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(is_null(Auth::user())) {
          return view('welcome');
        }

        $user = User::find(Auth::user()->id);

        return view('welcome', compact('user'));
    }
}
