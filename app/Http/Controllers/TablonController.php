<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tablon;
use Auth;
use App\Card;
use Alert;

class TablonController extends Controller
{
    public function __construct() {
      $this->middleware('auth');
    }
    public function index() {
      //
    }

    public function show($id) {
      $tablon = Tablon::find($id);

      if(is_null($tablon)) {
        toast('El tablon que has intentado acceder no existe.', 'error', 'top-right');
        return redirect()->route('home');
      }
      if($tablon->user_id != Auth::user()->id) {
        toast('No tienes permiso para acceder a este tablon.','error','top-right');
        return redirect()->route('home');
      }

      return view('tablones.show', compact('tablon'));
    }

    public function update(Request $request) {
      $card = Card::find($request->card_id);

      if($request->has('title') && $request->has('description')) {
        $card->title = $request->title;
        $card->description = $request->description;
        $card->update();
      } else {
        toast('Hubo un error a la hora a de actualizar el tablon', 'error', 'top-right');
      }

      return back();
    }

    public function create() {
      return view('tablones.create');
    }

    public function store(Request $request) {
      $tablon = new Tablon();

      $tablon->title = $request->title;
      $tablon->description = $request->description;
      $tablon->imgurl = $request->imgurl;
      $tablon->user_id = Auth::user()->id;
      $tablon->save();

      return redirect()->route('home');

    }
}
