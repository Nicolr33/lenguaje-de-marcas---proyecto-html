<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tablon extends Model
{
    protected $table = 'tablones';

    public function cards() {
      return $this->hasMany('App\Card', 'tablon_id');
    }
    
}
