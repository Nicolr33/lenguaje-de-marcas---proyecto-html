<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    public function elements() {
      return $this->belongsToMany('App\Element', 'element_card');
    }
}
