# Trello Kibou
![GIF Movimiento de una tarjeta](https://i.gyazo.com/e12f20e97c1d7350ec711363a4efc83e.gif)

### Explicación
------
Es un clon de Trello incompleto, solamente es posible iniciar sesión, registrarse, crear tablones y editar tarjetas dentro de ellos. 
> Backend: PHP (Laravel) - Frontend: Bootstrap

### Instalación
------
Clonar el repositorio:
```bash
git clone https://gitlab.com/Nicolr33/lenguaje-de-marcas---proyecto-html.git
```

Tienes que añadir los datos de tu base de datos en el .env y ejecutar:
```bash
php artisan migrate
```
### Iniciar
------
```bash
php artisan serve
```
Tienes que acceder a localhost:8080

## DEMO
[Trello Kibou](https://trello.kibou.es/).

### Usuarios de prueba (DEMO)
Sin tablones:  
Email: prueba@prueba.com  
Contraseña: test1234  

Con Tablones:  
Email: prueba1@prueba.com  
Contraseña: test1234  
