@extends('layout.master')

@section('content')
<br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="form-area">
        <form method="POST" action="{{route('storeTablon')}}">
          @csrf
          <br style="clear:both">
          <h3 style="margin-bottom: 25px; text-align: center;">Crear tablon</h3>
          <div class="form-group">
            <input type="text" class="form-control" name="title" placeholder="Titulo" required>
          </div>
          <div class="form-group">
            <textarea class="form-control" type="textarea" name="description" placeholder="Descripcion" ></textarea>
          </div>
          <div class="form-group">
            <input type="url" class="form-control" id="email" name="imgurl" placeholder="URL de la Imagen">
          </div>
          <div class="text-center">
              <button type="submit" class="btn btn-primary pull-right">Crear tablon</button>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>
@stop
