@extends('layout.master')

@section('content')
<div class="bgtablon">
  <br>
  <h2 class="text-center">{{$tablon->title}}</h2>
  <div class="container-fluid">
    <div class="row">
      @foreach($tablon->cards as $card)
      <div class="col-md-2" id="dxy">
        <div class="card" style="width: 18rem;">
          @if(!empty($card->imgurl))
          <img class="card-img-top" src="{{$card->imgurl}}">
          @endif
          <div class="card-body text-center">
            <h5 class="card-title text-center">{{$card->title}}</h5>
            <p class="card-text">{{$card->description}}</p>
            <hr class="my-2">
            @foreach($card->elements as $element)
            <a href="#">
              <div class="element">
                <span>{{$element->title}}</span>
              </div>
            </a>
            <hr class="my-2">
            @endforeach
            <div class="element">
              <a href="#">
                <span style="color:#BD2031;">Añadir elemento</span>
              </a>
              <hr class="my-2">
            </div>
            <button data-toggle="modal" data-target="#{{$cosa = str_replace(' ', '_', $card->title . '_'. $card->id)}}" class="btn btn-primary">Editar</button>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>

@foreach($tablon->cards as $card)
<div class="modal fade" id="{{$cosa = str_replace(' ', '_', $card->title . '_'. $card->id)}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Configuración de la tarjeta <strong>{{$card->title}}</strong></h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{route('updateCard')}}">
          @csrf
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nombre:</label>
            <input type="text" class="form-control" name="title" placeholder="Introduce el titulo" value="{{$card->title}}">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Descripcion:</label>
            <textarea class="form-control" id="message-text" value="{{$card->description}}" name="description">{{$card->description}}</textarea>
            <input type="hidden" name="card_id" value="{{$card->id}}">
          </div>
          <div class="modal-footer" style="color: white;">
            <a class="btn btn-secondary" data-dismiss="modal">Cerrar</a>
            <button role="submit" class="btn btn-primary">Guardar cambios</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>
@endforeach



@stop
