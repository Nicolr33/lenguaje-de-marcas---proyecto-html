@extends('layout.master')
@section('content')
  <section class="jumbotron text-center">
    <div class="container">
      <h1 class="jumbotron-heading">Bienvenido a Trello</h1>
      <p class="lead text-muted">Te ofrecemos la sensualidad de crear tu tablon para planificarte.</p>
      @guest
      <p>
        <a href="{{route('login')}}" class="btn btn-primary my-2">Accede a nuestra plataforma</a>
      </p>
      @endguest
    </div>
  </section>
  <div class="container">
    <div class="card-deck mb-3 text-center">
          @guest
            <div class="card mb-4 box-shadow">
              <div class="card-header">
                <h4 class="my-0 font-weight-normal">Gratis</h4>
              </div>
              <div class="card-body">
                <h1 class="card-title pricing-card-title">0e / mes</h1>
                <ul class="list-unstyled mt-3 mb-4">
                  <li>1 GB de espacio</li>
                  <li>10 tablones</li>
                  <li>2 widgets</li>
                </ul>
                <a href="{{route('register')}}" class="btn btn-lg btn-block btn-outline-primary">Registrate</a>
              </div>
            </div>
            <div class="card mb-4 box-shadow">
              <div class="card-header">
                <h4 class="my-0 font-weight-normal">Pro</h4>
              </div>
              <div class="card-body">
                <h1 class="card-title pricing-card-title">15e <small class="text-muted">/ mes</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                  <li>Opcion a crear un grupo</li>
                  <li>10 GB de espacio</li>
                  <li>Soporte 24/7</li>
                </ul>
                <a class="btn btn-info btn-lg btn-block btn-primary" role="button">Compralo ahora</a>
              </div>
            </div>
          @endguest
          </div>
          @auth
            @if($user->tablones->count() != 0)
            <h1>Tus tablones</h1>

            <div class="row">

              @foreach($user->tablones as $tablon)
              <div class="col-md-4">
                <div class="card-deck mb-3 text-center">
                  <div class="card mb-4 box-shadow">
                    <img class="card-img-top" src="{{$tablon->imgurl}}" width="286px" height="180px" alt="Card image cap">
                    <div class="card-header">
                      <h4 class="my-0 font-weight-normal">{{$tablon->title}}</h4>
                    </div>
                    <div class="card-body">
                      <p>{{$description = substr($tablon->description, 0, 80)}}...</p>
                      <a href="{{route('tablon', $tablon->id)}}" class="btn btn-lg btn-block btn-outline-primary">Acceder</a>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach


            </div>
            @else
              <h1 class="text-center">No hay tablones. Crea <a href="{{route('createTablon')}}">uno</a></h1>
              @endif
          @endauth
  </div>

@stop
