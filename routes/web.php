<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');
Auth::routes();

Route::get('tablon/{id}', 'TablonController@show')->name('tablon');

Route::post('update/card', 'TablonController@update')->name('updateCard');
Route::get('create/tablon', 'TablonController@create')->name('createTablon');
Route::post('store/tablon', 'TablonController@store')->name('storeTablon');
